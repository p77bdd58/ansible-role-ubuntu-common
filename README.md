ubuntu-common
=============

Secure & configure server with:  
- system
- ssh
- fail2ban
- ufw

Additional vagrant configuration:
- wondershaper for spoofing slow connection
- virtualbox fix for nginx static file issue
- DNS resolver with OpenDNS


Requirements
------------

Run this role as sudo!


Role Variables
--------------

```
is_vagrant: true,
sys: true,
sys_hostname: server,
fail2ban: true,
fail2ban_dest_email: "office@bcooling.com.au",
sshd: true,
sshd_max_auth_tries: 2,
ufw: true,
ufw_reject: { attacker: { port: 22, ip: 192.168.22.80 }
```

Dependencies
------------

No dependencies.


Example Playbook
----------------

```
- hosts: servers
  roles:
     - { role: bencooling.common, sys_hostname: maestro, is_vagrant: true }
```


License
-------

BSD


Author Information
------------------

[bcooling.com.au](bcooling.com.au)
